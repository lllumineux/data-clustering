import math
from typing import Union


class Cluster:
    def __init__(self, x: Union[int, float], y: Union[int, float], name: str = ''):
        self.x = x
        self.y = y
        self.name = name

    def get_own_points(self, points):
        return list(filter(lambda x: x.cluster == self, points))


class Point:
    def __init__(self, x: Union[int, float], y: Union[int, float], name: str = '', cluster: Cluster = None):
        self.x = x
        self.y = y
        self.name = name
        self.cluster = cluster

    def get_distance_to_cluster(self, cluster):
        if cluster is None:
            return float('inf')
        return math.sqrt((self.x - cluster.x) ** 2 + (self.y - cluster.y) ** 2)
