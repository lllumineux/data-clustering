import csv
import random

import plotly.express as px

from classes import Cluster, Point
from config import CLUSTER_AMOUNT


def main():
    points = []

    with open('../data/wines.csv', newline='') as csvfile:
        transactions_reader = csv.reader(csvfile, delimiter=',')
        _ = next(transactions_reader)  # workaround to omit the header

        for row in transactions_reader:
            name, country, region, winery, rating, number_of_reviews, price, year = row
            if float(price) <= 1000:
                points.append(Point(float(price), float(rating), name=name))

    max_x, max_y = max(p.x for p in points), max(p.y for p in points)

    clusters = [
        Cluster(
            random.uniform(0.0, max_x),
            random.uniform(0.0, max_y),
            name=f'Type {i + 1}'
        ) for i in range(CLUSTER_AMOUNT)
    ]

    while True:
        is_changed = False

        for point in points:
            for cluster in clusters:
                if point.get_distance_to_cluster(cluster) < point.get_distance_to_cluster(point.cluster):
                    point.cluster = cluster

        for cluster in clusters:
            cluster_points = cluster.get_own_points(points)
            if cluster_points:
                avg_x = sum(point.x for point in cluster_points) / len(cluster_points)
                avg_y = sum(point.y for point in cluster_points) / len(cluster_points)
                if any([cluster.x != avg_x, cluster.y != avg_y]):
                    cluster.x, cluster.y = avg_x, avg_y
                    is_changed = True

        if not is_changed:
            break

    points.sort(key=lambda x: int(x.cluster.name.split()[1]))
    df = [{'Price': p.x, 'Rating': p.y, 'Type': p.cluster.name, 'Name': p.name} for p in points]
    fig = px.scatter(df, x='Price', y='Rating', color='Type', hover_name='Name')
    fig.show()


if __name__ == '__main__':
    main()
